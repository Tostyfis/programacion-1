
//MAL EJEMPLO
//===========================================================
#include <iostream>
using namespace std;

#define PI 3.1416; //Definimos una constante llamada PI

int main()
{
    cout << "Mostrando el valor de PI: " << PI;

    return 0;
}


//BUEN EJEMPLO
//===========================================================

#include <iostream>
using namespace std;

int main()
{
    const float PI  = 3.1416; //Definimos una constante llamada PI
    cout << "Mostrando el valor de PI: " << PI << endl;

    return 0;
}