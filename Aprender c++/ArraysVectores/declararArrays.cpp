/*
Para declarar un vector en C++, se deben seguir las mismas normas básicas que se siguen para declarar una variable cualquiera, 
con un pequeño cambio en la sintaxis. Para declarar un vector, arreglo o como lo quieras llamar, necesitaremos saber el tipo 
de los datos que irán al interior de este, es decir, serán número enteros, o numero decimales o cadenas de texto, etc. necesitamos 
también, como siempre, un nombre para el vector y un tamaño máximo. La sintaxis para declarar un vector en C++ es la siguiente:


tipo_de_dato nombre_del_vector[tamanio];

Tenemos entonces, tal como mencioné antes, que para declarar un vector en C++, debemos definirle un tipo de los datos, sea entero, 
float, string, etc., debemos darle un nombre y al interior de los corchetes "[]" debemos poner el tamaño máximo que tendrá el vector, 
es decir la cantidad máxima de datos que podrá contener (recuerda que en C++ esto es necesario hacerlo). Veamos un ejemplo en el
cual pondré la declaración de varios vectores de diferentes tipos y tamaños en C++.

==================================================================================================================================

Declaración de un Array o Vector en C++

int my_vector1[10];
float my_vector2[25];
string my_vector3[500];
bool my_vector4[1000];
char my_vector5[2];

===================================================================================================================================

Forma 1 de declarar un Array o Vector en C++

string vector[5] = {"5", "hola", "2.7", "8,9", "adios"};

Aquí hemos declarado un vector de tipo string tamaño 5 y lo hemos inicializado con diferentes valores, es necesario notar que cada 
valor va entre comillas dobles "" puesto que son strings. El valor inicial corresponde a la casilla o índice 0 y tiene el valor de "5", 
el índice 1 el valor es "hola" y el índice 4 el valor es "adiós", es importante notar que el primer índice de n array o vector no es el 
UNO sino que es el CERO.

Forma 2 de declarar un Array o Vector en C++

int vector2[] = {1,2,3,4,10,9,80,70,19};

Aquí hemos declarado un vector de tipo int y no especificamos su tamaño, si el tamaño no se especifica entre los corchetes, 
el vector tendrá como tamaño el número de elementos incluidos en la llave, para este caso es 9.



==================================================================================================================================

Obtener el valor de una casilla específica en un array en C++
Es muy común el caso en el que tenemos un vector con una enorme cantidad de elementos, sin embargo de todos estos, solo nos interesa 
uno en especial y corremos con la suerte de saber cuál es su índice, sabiendo el índice de un elemento en un array es bastante sencillo 
obtener el valor de este:


float vector4[5] = {10.5, 5.1, 8.9, 10, 95.2}; //Array con 5 elementos
float numero5 = vector4[4]; //Para acceder al elemento 5, se usa el índice 4
float primerNumero = vector4[0]; //Para el primer elemento se usa el índice 0

=====================================================================================================================================

Recorrer un Array o Vector en C++

#include "iostream"

using namespace std;

int main()
{
    int edades[] = {1,2,9,8,16,32,9,50,36,20,1,87};
    int limite = (sizeof(edades)/sizeof(edades[0]));
    for (int i = 0; i < limite; i++)
    {
            cout<<edades[i]<<endl;
    }
}

Línea 2:
En la segunda línea, tenemos la declaración del límite del ciclo o en otras palabras el tamaño del array. El tamaño de un array 
se puede calcular de varias formas, aquí lo obtenemos calculando el tamaño del array entero, dividido por el tamaño del primer 
elemento de dicho array, para mas detalles de esto, verifica la información sobre el operador sizeof.


*/