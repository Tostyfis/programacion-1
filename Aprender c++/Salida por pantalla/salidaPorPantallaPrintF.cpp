#include "iostream"

using namespace std;

int main()
{
    //Se muestra un mensaje por pantalla.
    printf("Hola Mundo");
    printf(" Desde ProgramarYa.");
    printf("\n");

    //Alternativamente
    printf("Hola Mundo Desde ProgramarYa.\n");

    return 0;
}
