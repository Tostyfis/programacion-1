/*

Para este ejercicio retomaré el ejemplo del artículo de arreglos o vectores: Queremos crear un programa con el cual podamos guardar los títulos y 
los autores de diferentes libros sin perder ninguno de ellos. El usuario es el encargado de suministrar la información de cada libro. En esta ocasión
ya sabemos usar punteros, así que será también el usuario quien nos diga cuántos libros desea ingresar, ya no necesitamos suponer que sólo ingresará 
5 libros. Veamos:

*/

#include "iostream"
#include "stdio.h"
#include "string"

using namespace std;

int main()
{
    string* titulos = NULL; //Se inicializa el puntero (inicia en null)
    string* autores = NULL; //Se inicializa el puntero (inicia en null)

    int tamanio ; //Se inicializa la variable

    cout << "Cuantos libros desea ingresar?";

    string entrada;

    getline(cin, entrada); //Se asigna el valor ingresado

    tamanio = stoi(entrada); //Se transforma la entrada en número

    //Declaramos un arreglo del tamaño ingresado para los titulos
    titulos = new string[tamanio];

    //Declaramos un arreglo del tamaño ingresado para los autores
    autores = new string[tamanio];

    cout << "Por favor ingrese la siguiente información de los Libros: \n";
    for(int i = 0; i < tamanio; i++)
    {
        cout << "\n******* Libro " << i + 1 << "********:\n";
        cout << "Titulo: ";
        //cin >> titulos[i]; //No funciona con espacios
        getline(cin, titulos[i]);
        cout << "Autor: ";
        //cin >> autores[i]; //No funciona con espacios
        getline(cin, autores[i]);
    }

    //Liberamos la memoria de ambos punteros
    delete [] titulos;
    delete [] autores;
    titulos = NULL;
    autores = NULL;

    system("pause");

    return 0;
}