/*

Los punteros y elementos dinámicos en C++ con ejemplos y ejercicios resueltos

*/

int variable; //Creamos un entero
int * apuntador = &variable;//Creamos una apuntador a la posición en memoria de "variable"
*apuntador = 20; //Le asignamos un valor a esa posición de memoria.

delete [] apuntador; //Después de operar con punteros es necesario liberar la memoria.
puntero = NULL;

/*

Ejemplo de apuntadores

*/

char *apuntador = NULL; //Declaramos un puntero
//Es recomendable inicializar un puntero en null, para detectar errores fácilmente

char letra; //Declaramos una variable primitiva

apuntador = &letra; //Asignamos al apuntador la dirección de memoria de la variable primitiva

*apuntador = 'x'; //Modificamos la variable a través del apuntador

cout << letra; //Muestra x por pantalla