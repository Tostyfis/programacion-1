//===========================================================
/*
Vamos a retomar el ejemplo anterior, donde queremos hacer que 
nuestro programa le pida a un usuario una serie de números cualquiera 
y que solo dejaremos de hacerlo cuando el usuario ingrese un número 
mayor a 100, una vez mas es un ejemplo sencillo con el que nos aseguraremos 
de haber comprendido bien todos los conceptos anteriores:
*/
//===========================================================
#include <iostream>
using namespace std;

int main()
{
    int numero;
    cout <<  "Ingrese un numero ";
    cin >> numero;
    while(numero <= 100)
    {
        cout <<  "Ingrese un numero ";
        cin >> numero;
    }
   
    return 0;
}