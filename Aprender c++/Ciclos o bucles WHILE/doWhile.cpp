//===========================================================
/*
Vamos a retomar el ejemplo anterior, donde queremos hacer que 
nuestro programa le pida a un usuario una serie de números 
cualquiera y que solo dejaremos de hacerlo cuando el usuario 
ingrese un número mayor a 100, una vez mas es un ejemplo sencillo 
con el que nos aseguraremos de haber comprendido bien todos los 
conceptos anteriores, vamos a ver cómo hacer lo mismo con dos 
tipos de ciclos diferentes (el while y el do-while), sin embargo
vamos a ver como con uno es más eficiente que con el otro:
*/
//===========================================================
#include <iostream>
using namespace std;
 

int main(){

    int numero;

    do{ //Esta línea es por decirlo así, la parte novedosa del ciclo do-while, esta expresión no evalúa ninguna condición ni nada, simplemente da paso directo al bloque de instrucción y luego permite la evaluación de la condición.

        cout <<  "Ingrese un numero ";

        cin >> numero;

    }

    while (numero <= 100);

        return 0;

    
    

}

/*
DIFERNCIAS ENTRE WHILE Y DO WHILE

La diferencia es que el do-while, primero ejecuta la acción y luego evalúa la condición, mientras que el while evalúa 
la condición antes que cualquier cosa. Esto quiere decir, que el ciclo while podría no ejecutar ni siquiera una vez 
lo que tenga en su interior, mientras que el do-while te asegura que lo que pongas dentro se ejecute mínimo una vez, 
pues primero ejecuta y luego evalúa la condición.


*/