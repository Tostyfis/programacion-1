/*

Declarar una matriz en C++ es muy similar a la de un vector, se deben seguir las mismas normas para declarar una variable pero 
una vez más con un pequeño cambio en la sintaxis. Primero necesitaremos saber el tipo de los datos que irán al interior de este
(números, decimales o cadenas de texto, etc.) necesitamos también, como siempre, un nombre para la matriz y un tamaño máximo 
tanto para las filas como para las columnas. La sintaxis para declarar una matriz en C++ es la siguiente:


tipoDato nombreMatriz[filas][columnas];

*/

int myMatriz1[10][5];
float myMatriz2[5][10];
// string myMatriz3[15][15];
bool myMatriz4[1000][3];

/*

Forma 1 de declarar una matriz

int myMatriz1[2][2] = {{1,2},{3,4}};

Aquí hemos declarado una matriz de tipo int de dos filas y dos columnas y la hemos inicializado con diferentes valores. 
El valor inicial corresponde a la casilla 0,0 (fila cero, columna cero) y tiene el valor de 1, en la fila cero columna uno 
tenemos el valor de 2, en la fila uno columna cero el valor de 3 y finalmente en la fila uno columna uno el valor de 4. Es 
importante notar que el primer tanto la fila como la columna comienzan desde cero y no desde uno, por esto la primer casilla 
corresponde a la fila y columna cero.

¡Bien! Ya sabemos cómo declarar una matriz en C++, sin embargo, aún no sabemos cómo acceder a los datos que estas contienen. Veámoslo:

Obtener el valor de una casilla específica
Para acceder al valor de una casilla nuevamente haremos uso de los corchetes, pero esta vez no para declarar tamaños (porque 
eso ya lo hicimos) sino para indicar posiciones (fila y columna).


int myMatriz1[2][2] = {{1,2},{1,1}}; //Matriz con 4 elementos
int fila1Casilla1 = myMatriz[1][1]; //Para acceder a la casilla 1,1 se usan dichos indices
int primerNumero = myMatriz[0][0]; //La primer casilla sie

Recorrer una matriz en C++
Para obtener todos los datos que se encuentran al interior de una matriz, debemos acceder a cada posición y esto se hace
fácilmente con dos ciclos for (anidados). La lógica de este procedimiento es la siguiente, el primer ciclo for comenzará
desde cero e ira hasta el número de filas, de modo que la variable de control que generalmente llamamos "i", será la que
va a ir variando entre cero y el tamaño del array, de esta forma al poner la i al interior de los corchetes, estaremos
accediendo al valor de cada fila y el segundo ciclo irá de cero al número de columnas y normalmente se usa la variable
llamada j para acceder a cada columna, veamos:

Nota: En el siguiente código uso una forma sencilla y rápida de obtener la cantidad o número de filas de una matriz y también
cómo obtener el número o cantidad de columnas de una matriz. Ten en cuenta que esto es importante, pues a veces no tenemos la
certeza del tamaño de la matriz.


#include <iostream>

using namespace std;

int main()
{
    int edades[3][2] = {{1,2},{9,8},{14,21}};
    int filas = (sizeof(edades)/sizeof(edades[0]));
    int columnas = (sizeof(edades[0])/sizeof(edades[0][0]));
    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < columnas; j++)
        {
            cout<<edades[i][j]<<endl;
        }
    }
}
*/