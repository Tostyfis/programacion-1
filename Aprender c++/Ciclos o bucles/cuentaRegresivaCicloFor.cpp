//===========================================================
/*
Ahora veremos otro ejemplo sencillo en cual veremos que el ciclo for también 
puede iterar disminuyendo el valor del contador, para comprender esto, lo que 
haremos será imprimir por pantalla una cuenta regresiva desde el número diez 
hasta el cero, veamos:
*/
//===========================================================
#include <iostream>
using namespace std;

int main(){

    cout << "================" << "\n" << "\n" ;

    cout << "Cuenta regresiva" << "\n" << "\n" ;

    cout << "================" << "\n" << "\n" ;

   for(int i = 10; i >= 0; i = i-1){ //Notemos que escribir i = i - 1  es similar a escribir i--

        cout << i << "\n" << "\n";

   }
   cout << "================" << "\n" << "\n" ;
   cout << "Ignicion" << "\n" << "\n";
   cout << "================" << "\n" << "\n" ;

   return 0;
}

