//===========================================================
/*
Vamos a retomar el ejemplo anterior, donde deseábamos sacar los 
números pares entre el numero 50 y el 100, es un ejemplo sencillo 
con el que nos aseguraremos de haber comprendido bien lo anterior:
*/
//===========================================================
#include <iostream>
using namespace std;

int main(){

    for(int i = 50; i <= 100; i = i+2){ // Tambien se puede escribir i+=2, en vez de i = i + 2, son lo mismo.

            cout << i << "\n" ;

    }
    return 0;
}

