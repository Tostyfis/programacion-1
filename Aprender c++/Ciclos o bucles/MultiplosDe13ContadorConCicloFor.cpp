//===========================================================
/*
Para este ejemplo haremos algo un poco más complejo. El ejemplo 
consiste en contar al interior de un ciclo for, cuántos números 
entre el 0 y el 10.000 son múltiplos del 13. Para ello haremos 
uso del operador % (modulo) que obtiene el residuo de una 
división y también usaremos un pequeño condicional para 
verificar que el modulo sea cero al dividir por 13.
*/
//===========================================================
#include "iostream"
#include "stdlib.h"

using namespace std;
int main()
{
    int contador = 0; //Iniciamos el contador en cero
    for(int i = 0; i < 10000; i++)
    {//Notemos que escribir i++ es similar a escribir i = i + 1
        if(i%13 == 0) //Si el residuo es cero es múltiplo de 13
        {
            contador++; //Si es múltiplo aumentamos el contador en 1
        }
    }
    //Mostramos el contador después de verificar todos los números
    cout << contador << endl;
    system("PAUSE");
    return 0;
}

/*

Este ciclo for nos permitirá saber que existen 770 múltiplos del 13 en los números del 0 al 10000.

*/