#include <stdio.h>

#include <limits.h>

int main(void){

    printf("\nBits of type char: %d\n\n", CHAR_BIT);

    printf("\nMax char: %d\n\n", CHAR_MAX);

    printf("\nMax char: %d\n\n", CHAR_MIN);

    return 0;

}