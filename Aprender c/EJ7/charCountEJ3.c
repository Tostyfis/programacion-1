#include <stdio.h>

/* Cuenta los carcatertes de entrada*/

/*Escribe un programa que copie su entrada a la salida, reemplazando cada cadena de uno o mas blancos por un solo blanco*/


main(){
    
    int currentChar, lastCharPrinted;

    while ((currentChar = getchar())!= EOF){

        if(currentChar != ' ' || lastCharPrinted != currentChar){

            putchar(currentChar);

        }

        lastCharPrinted = currentChar;
    }
    
    
}

