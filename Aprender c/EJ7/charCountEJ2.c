#include <stdio.h>

/* Cuenta los carcatertes de entrada*/

/*Escribe un programa que copie su entrada a la salida, remplazando cada cadena tabulacion por /t, cada retroseso por /b cada diagonal por \\*/


main(){
    
    int currentChar;

    while ((currentChar = getchar())!= EOF){


        if (currentChar == '\t'){
            putchar('\\');
            putchar('t');
        }
        else if (currentChar == '\b'){
            putchar('\\');
            putchar('b');
        }
        else if (currentChar == '\\'){
            putchar('\\');
            putchar('\\');
        }else
        {  
        putchar(currentChar);
        }
        
        
    }
    
    
}

