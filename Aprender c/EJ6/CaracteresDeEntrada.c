#include <stdio.h>

/* Cuenta los caracteres de entrada*/

main(){

    long nc; // Declaracion de variables LONG nos permite tener muchos valores. Es como el INT pero para ver otro tipo de declareciones

    nc = 0; 

    while (getchar() != EOF) //Obtenemos los caracteres y lo comparamos con End Of File
    {

        ++nc; //Mientras sea distinto sumamos uno a nc Number of Character 

        printf("%ld\n", nc);
    }
    
}