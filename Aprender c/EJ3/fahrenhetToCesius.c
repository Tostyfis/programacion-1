
/* Convertir Far to Cel
Tarea 
Modifica el codigo para que imprima un encabezado encima de la tabla, indicando que es cada columna.
Escribe un programa que haga la conversion inversa, en lugar de Farenhet a Cel, de Cel a fahr.
*/

main(){ 

    int fahr, celcius; 

    int lower, upper, step;

    lower = 0; 

    upper = 300; 

    step = 20; 

    celcius = lower;

    printf( "Celcius\tFahrenheit\n");

    while (celcius <= upper){ 
        
        fahr = (celcius*9/5)+32;

        printf( " %3d\t%d\n ", celcius, fahr); 

        celcius = celcius + step;
    }

}

