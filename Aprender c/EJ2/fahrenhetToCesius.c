
/* Convertir Far to Cel
Tc= Temperatura en grados
Tf= Temperatura en fahrenheit
Tc = Tf -32 * 5 / 9


Mostar una tabla con Tf y Tc
Imprimir una tabla de equivalencia entre Far y cel la tabla va de 20 en 20,
desde 0 hasta 300
*/

main(){ 

    //Definicion de variables

    int fahr, celcius; 

    int lower, upper, step;

    lower = 0; // Valor mas vajo

    upper = 300; // Valor mas alto

    step = 20; // Valor de salto

    fahr = lower;

    while (fahr <= upper){ // While = mientras que (far sea MENOR O IGUAL QUE upper (Valor maximo) repite lo que hay debajo)
        
        celcius = 5*(fahr-32)/9;

        printf( " %3d\t%d\n ", fahr, celcius); // %d pone una variable entera, si pongo %3d hago que mi numero ocupe 3 caracteres/ si pionemos un %f se representas numeros con coma flotante

        fahr = fahr + step;
    }

}

