#include <stdio.h>

/*
Copia un archivo caracter a caracter 

lee un carcter 
while (caracter no sea final de archivo)
        manda a salida el carcter recien leido
        lee un carcter


*/

main(){
    int c;

    c = getchar(); // Coghe un caracter 

    while (c!= EOF){ // End of file busqueda del final del archivo ES UNA CONSTANTE!!!!
        putchar(c); // Mustra un caracter por pantalla

        c = getchar();
    }
    
}
