
//Constantes simbolicas 

#define LOWER 0
#define UPPER 300
#define STEP  20



main(){

    int fahr;

    for (fahr = LOWER; fahr <= UPPER; fahr = fahr + STEP)
        printf("%3d\t%6.1f\n",fahr, (5.0/9.0) * (fahr-32) );

}

/*
====Estructura bloque for====
for (

    fahr = 0; Asignacion                    Inicializamos la varible
    fahr <= 300; Condicion                  Asignamos valor maximo
    fahr = fahr + 20) Incremento            Incremento que quiro a cada

    Mientras que far sea menor o igual a 300 se cumple la condicion

*/