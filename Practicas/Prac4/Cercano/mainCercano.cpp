// ---------------------------------------------------
// 
// mainCercano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "El punto mas cercano es el "<< "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
// Lista<Punto> -> cercano() --> punto
// ---------------------------------------------------
void cercano(Punto * listaPuntos, const int cuantos){

Punto origen; //Primero establecemos el punto origen

Punto cerca; //Lugo creamos una varible para guardar el punto mas cercano e imprimirlo por pantalla

double guardadoTemporal = 0; //Creamos una ranuara de guardado para almacenar distancias de manera temporal

double distancias = 0; //Creamos una ranura de guardado para almacenar distancias definitivas

for(int i = 0; i <= cuantos -1; i++){ //Recorremos la lista 

  guardadoTemporal = origen.distancia(listaPuntos[i]); //Guardamos siempre al empezar el bucle la distancia

  if(guardadoTemporal > distancias){ //Entramos en este condicional que nos permite guardar en la varible distancia, esto esta hecho por que el ejercio nos pide el punto mas cercano

    distancias = guardadoTemporal;

  }
  
  if(guardadoTemporal < distancias){ //Condicional si el guardado temporal es mas pequenyo que el numero que hay en distancias guardamos el numero en esta varible mencionada

    distancias = guardadoTemporal;

    cerca = listaPuntos[i]; //Guardamos el punto mas cercano 

  }

}

muestraPunto(cerca); //Lo mostramos por pantalla

}
// ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto listaDePuntos[5]; //Definimos la lista de puntos

  leeListaPuntos(& listaDePuntos[0],4); //Intoducimos los numeros en la lista

  cercano(& listaDePuntos[0],4); //Llamamos a la funcion cercano

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
