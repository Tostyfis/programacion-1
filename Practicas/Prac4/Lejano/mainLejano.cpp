// ---------------------------------------------------
// 
// mainLEjano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
//   punto punto ->
// lejano()
//   --> punto
// ---------------------------------------------------

void lejano( const Punto & p1, const Punto & p2){

  Punto origen; //Definimos el punto origen

  double distanciaPunto1 = origen.distancia( p1 ); //Guardamos la distancia al origen del punto 1

  double distanciaPunto2 = origen.distancia ( p2 ); //Guardamos la distancia al origen del punto 2

  if(distanciaPunto1 > distanciaPunto2){ //Comparamos que distancia es mayor para determinar que punto es el mas lejano

    muestraPunto(p1);

  }

  if(distanciaPunto2 > distanciaPunto1){

    muestraPunto(p2);

  }

  if(distanciaPunto1 == distanciaPunto2){

    muestraPunto(p1);

    muestraPunto(p2);

    std::cout << " Tienen la misma distancia " << distanciaPunto1 << " = " << distanciaPunto2 << "\n";

  }

}// ()
// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto origen;

  Punto p1 = leePunto();

  Punto p2 = leePunto();

  lejano(p1, p2);

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
