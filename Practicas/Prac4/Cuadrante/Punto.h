// ---------------------------------------------------
//
// Punto.h
//
// ---------------------------------------------------

#ifndef PUNTO_YA_INCLUIDO
#define PUNTO_YA_INCLUIDO

// ---------------------------------------------------
#include <iostream>

// ---------------------------------------------------
// ---------------------------------------------------
class Punto { //Definimos en la parte privada x,y
 private:
  double x;
  double y;

 public:

  Punto(); //En la clase plublica se define la funcion punto
  
  Punto( const double, const double ); //Con sus respectivos tipos de valores
  
  double getX() const; //Definimos la funcion get X
  
  double getY() const; //Definimos la funcion get Y
  
  double distancia( const Punto & ) const;
  
  Punto suma( const Punto & ) const;
  
}; // class

// ---------------------------------------------------
#endif

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
