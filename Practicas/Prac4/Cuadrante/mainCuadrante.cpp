// ---------------------------------------------------
// 
// mainCuandrante.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------
void muestraPunto( const Punto & p ) { //La funcion mostrar punt recibe las coordenadas x,y para posteriormente representar el punto 

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------
Punto leePunto() { //Recoge el valor x,y para crear un nuevo punto

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;
} // ()
// --------------------------------------------------
//   punto ->
// queCuadrante()
//   --> N
// ---------------------------------------------------
double  queCuadrante(const Punto & p){

  double x1 = p.getX(); //Al igual que en la funcion muestraPunto, obtengo X y lo guardo 

  double y1 = p.getY(); //Lugo guardo Y

  double res = 0; //Cero la varible resultado 

  if(x1 >= 0 && y1 >= 0){ //Comienzo a anilizar X e Y, dependiendo de su tamanyo puedo identificar el cuadrante en el que estan

    res = 1;

  }

  if(x1 < 0 && y1 >= 0){

    res = 2;

  }

  if(x1 < 0 && y1 < 0){

    res = 3;

  }

  if(x1 >= 0 && y1 < 0){

    res = 4;

  }

  return res;

}// ()
// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------
void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();
  
  } // for
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto origen;

  Punto p1 = leePunto();

  double dist = origen.distancia( p1 );

  std::cout << " la distancia de p1 al origen es " << dist << "\n";

// ---------------------------------------------------
// queCuadrante()
// ---------------------------------------------------

  double cuadrante = queCuadrante(p1); // Introducimos el punto 1 en la funcion encargada de determinar el cuadrante

// ---------------------------------------------------
// Test
// ---------------------------------------------------

  if(cuadrante == 3){ //Si el cuadrante es el tres todo esta correcto

    std::cout << "OK, el punto introducido pertenece al cuadrante " << cuadrante << "\n";

  }else{ //En caso contrario no salta un mensaje de error y nos indica el cuadrante para poder determinar el error

    std::cout << "ERROR el punto introducido pertenece al cuadrante" << cuadrante <<"Y no al 3" << "\n";

  }

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
