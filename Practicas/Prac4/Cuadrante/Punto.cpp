// ---------------------------------------------------
//
// Punto.cpp
//
// g++ -c Punto.cpp 
//
// ---------------------------------------------------
#include <math.h>
#include "Punto.h"

// ---------------------------------------------------
// ---------------------------------------------------

Punto::Punto() //Definimos el contructor de la clase punto

  : x(0.0), y(0.0) //Se encarga de iniciar las variables x,i

{ }

// ---------------------------------------------------
// ---------------------------------------------------

Punto::Punto( const double xv, const double yv)

  : x(xv), y(yv) //Definimos otro constructor que le pasa a la clase punto x,y

{ }

// ---------------------------------------------------
// ---------------------------------------------------

double Punto::getX() const { //Funcion que nos permite coger la clase punto el valor x

  return (*this).x;

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

double Punto::getY() const { //Funcion que nos permite coger la clase punto el valor y

  return (*this).y;

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

double Punto::distancia( const Punto & otro ) const { //La funcion distancia toma un punto y calcula la distancia con 

  double dx = (*this).x - otro.x;                     //Respecto al origen devolviendo un resultado.

  double dy = (*this).y - otro.y;

  return sqrt( dx*dx + dy*dy );

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
Punto Punto::suma( const Punto & otro ) const { //La funcion suma recibe un punto y devulve la suma de este 

  Punto nuevo( (*this).x+otro.x, (*this).y+otro.y );//Proporcionando otro punto.

  return nuevo;
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
