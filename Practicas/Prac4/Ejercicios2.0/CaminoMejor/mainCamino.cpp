// ---------------------------------------------------
// 
// mainCercano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include <math.h>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "El punto mas cercano es el "<< "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
// Lista<Punto> -> cercano() --> punto
// ---------------------------------------------------

double camino(Punto * pLista, const int cuantos){

  Punto origen (0,0); //Ahora definimos el punto origen para realizar calculos

  double distancias[10]; //A continucion definimos la lista distancias para guardar la distancias calculadas

  double resX = 0;

  double resY = 0;

  int b = 1;

  double res = 0;

  for(int i = 0; i < cuantos-1; i++){ //Iniciamos un bucle 

    resX = pLista[b].getX() - pLista[i].getX(); //ahora tomamos dos puntos, y restamos al segundo el primero y tenemos el resultado X

    resY = pLista[b].getY() - pLista[i].getY(); //ahora tomamos dos puntos, y restamos al segundo el primero y tenemos el resultado Y

    distancias[i]= sqrt( resX * resX + resY * resY ); //A continuacion realizamos la siguiente ecuacion

    b++;

    res = distancias[i] + res; //Y por ultimo vamos sumando las distancias para obtener el camino

  }

  return res;

}// ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto lista[3]; //Primero definimos la lista de puntos

  leeListaPuntos(&lista[0],3);

  double resFinal = camino(&lista[0],3);

  std::cout << "El camino es "<< resFinal << "\n";

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
