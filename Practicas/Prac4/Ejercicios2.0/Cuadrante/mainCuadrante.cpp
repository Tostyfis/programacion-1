// ---------------------------------------------------
// 
// mainCuandrante.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------
void muestraPunto( const Punto & p ) { //La funcion mostrar punt recibe las coordenadas x,y para posteriormente representar el punto 

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------
Punto leePunto() { //Recoge el valor x,y para crear un nuevo punto

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;
} // ()
// --------------------------------------------------
//   punto ->
// queCuadrante()
//   --> N
// ---------------------------------------------------
int queCuadrante(const Punto & p){

  int x = p.getX();

  int y = p.getY();

  int cuadrante = 0;

  if(x >= 0 && y >= 0){

    cuadrante = 1;

  }if(x >= 0 && y <= 0){

    cuadrante = 2;

  }if(x <= 0 && y <= 0){

    cuadrante = 3;

  }if(x <=0 && y >= 0){

    cuadrante = 4;

  }

  return cuadrante;
}


// ()
// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------
void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();
  
  } // for
} // ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto origen;

  Punto p1 = leePunto();

  double dist = origen.distancia( p1 );

  std::cout << " la distancia de p1 al origen es " << dist << "\n";

// ---------------------------------------------------
// queCuadrante()
// ---------------------------------------------------
  double res = queCuadrante( p1 );

  if(res == 3){

    std::cout<< "OK" << "\n";

  }else{
    std::cout<< "ERROR" << "\n";
  }
  
}// ()
  
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
