// ---------------------------------------------------
// 
// mainCercano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "El punto mas cercano es el "<< "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
// Lista<Punto> -> cercano() --> punto
// ---------------------------------------------------

double cercano(Punto * pLista, const int cuantos){

  Punto origen (0,0);

  double distancia[10];

  double menorDistanciaRegistrada = 11111;

  Punto masCercano (0,0);


  for( int i=0; i<=cuantos-1; i++) {

	  distancia[i] = origen.distancia(pLista[i]);

    //std::cout << "Distancia del punto " << distancia[i] << "\n";

      if (distancia[i] < menorDistanciaRegistrada){

        menorDistanciaRegistrada = distancia[i];

        masCercano = pLista[i];

    }

  } // for
 
  muestraPunto(masCercano);

}// ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto lista[4];

  leeListaPuntos(&lista[0],4);

  cercano(&lista[0],4);

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
