// ---------------------------------------------------
// 
// mainLEjano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
//   punto punto ->
// distanciaMayor()
//   --> punto
// ---------------------------------------------------

double distanciaMayor(const Punto & p, const Punto & a){

  
  //-----------------------------------------------------------------

  Punto origen( 0, 0);

  //-----------------------------------------------------------------

  double dist1 = origen.distancia( p );

  double dist2 = origen.distancia( a );

  
  //-----------------------------------------------------------------

  if(dist1 > dist2){

    muestraPunto(p);

  }else{
    
    muestraPunto(a);

  }

  //-----------------------------------------------------------------

}

// ()
// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto p1 = leePunto();

  Punto p2 = leePunto();

  double mayorDistancia = distanciaMayor(p1,p2);

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
