// ---------------------------------------------------
// 
// mainCercano.cpp
// 
// g++  Punto.cpp main.cpp 
// 
// ---------------------------------------------------
#include <iostream>

#include <math.h>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "El punto mas cercano es el "<< "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {

  double nx;

  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;

} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// --------------------------------------------------
// Lista<Punto> -> cercano() --> punto
// ---------------------------------------------------

double camino(Punto * pLista, const int cuantos){

  Punto origen (0,0);

  double distancias[10];

  double resX = 0;

  double resY = 0;

  int b = 1;

  double res = 0;

  for(int i = 0; i <= cuantos-1; i++){ 

    resX = pLista[b].getX() - pLista[i].getX();

    resY = pLista[b].getY() - pLista[i].getY();

    distancias[i]= sqrt( resX * resX + resY * resY );

    b++;

    std::cout <<  distancias[i] << "\n";

  }

  for (int n = 0; n <= cuantos-1; n++){

    res = distancias[n] + res;


  }

  return res;

}// ()

// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto lista[4];

  leeListaPuntos(&lista[0],4);

  double resFinal = camino(&lista[0],4);

  std::cout << "El camino es "<< resFinal << "\n";

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
