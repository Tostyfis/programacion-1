//-------------------------------------------------------------------------------------------
//
//Programa; sumatorio bucle
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
// La función sumatorio() recibe un número natural n y devuelve la suma
//
//Diseño;
//
// Natural --> suamtorio() ---> Natural
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion sumatorio()
//-------------------------------------------------------------------------------------------
int sumatorio(int numero){ //Definimos la varible numero, que es el numero que le pasamosdesde la llamada de main

    int res = 0;

    for(int i = 0; i <= numero; i++){//Iniciamos un bucle (Hasta que la varible i sea menor o igual que el valor de nuemero)

        res = i + res; //repetimos esta operacion

    }

    return res;

}
//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
    void test(int resultado){

        if(resultado =! 6){

            cout << "Error";

        }else{

            cout << "OK";

        }
    
    }
//-------------------------------------------------------------------------------------------
int main(){

    int numero = 3; //Definimos el numero que mandamos a la funcion sumatorio

    int res = 0;

    res = sumatorio(numero);

    test(res); //Le mandamos el resultado a la funcion test.

}