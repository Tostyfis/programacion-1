//-------------------------------------------------------------------------------------------
//
//Programa; sumatorio pares
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
//Piensa un algoritmo para la función sumatorioPares() que utilice una repetición pero no una condición dentro de ella.
//
//Diseño;
//
// Natural --> suamtorioPares() ---> Natural
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion sumatorio()
//-------------------------------------------------------------------------------------------
int sumatorioPares(int numero){ 

    int res = 0;

    for(int i = 0; i <= numero; i++){ //Creamos el bucle como se indica

        res = res + ( i * (i + 1)/10); //esta vez sumamos a res el resultado que nos de la ecuacion, de esta solo sequeda la parte par.

    }

    return res;

}
//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
void test(int resultado){

    if(resultado =! 3){

        cout << "Error";

    }else{

        cout << "OK";

    }
    
}
//-------------------------------------------------------------------------------------------
int main(){

    int numero = 4; 

    int res = 0;

    res = sumatorioPares(numero);

    test(res); 

}

