//-------------------------------------------------------------------------------------------
//
//Programa; sumatorio
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
// La función sumatorio() recibe un número natural n y devuelve la suma
//
//Diseño;
//
// Natural --> suamtorio() ---> Natural
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion sumatorio()
//-------------------------------------------------------------------------------------------
int sumatorio(int numero){ //Definimos la varible numero, que es el numero que le pasamosdesde la llamada de main

    int res;

    res = (numero * (numero + 1)) / 2; //Y guardamos en res la ecuacion del sumatorio

    return res;

}
//-------------------------------------------------------------------------------------------
//Funcion main()
//-------------------------------------------------------------------------------------------
int main(){

    int numero = 3; 

    sumatorio(numero);

}