//-------------------------------------------------------------------------------------------
//
//Programa; esPositivo
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
// La función esPositivo() recibe un número entero y devuelve verdadero si es mayor o igual que cero; o falso en caso contrario. Su diseño es el siguiente:
//
//Diseño;
//
// Entero --> esPositivo() ---> V/F
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion esPositivo()
//-------------------------------------------------------------------------------------------
bool esPositivo(int numero){ 

    if(numero >= 0){ //Si el numero que le hemos pasado es mayor o igual a 0, quiere decir que es positivo, devolvemos TRUE

        return true;

    }else{ //En los demas casos devolvemos FALSE

        return false;

    }


}
//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
    void test(bool resultado){

        if(resultado != 0){ //Si el resutado es distinto de 0 lanzamos Error

            cout << "Error";

        }else{

            cout << "OK";

        }
    
    }
//-------------------------------------------------------------------------------------------
//Funcion main()
//-------------------------------------------------------------------------------------------
int main(){

    int numero = -5;

    bool res;

    res = esPositivo(numero); //Asignamos al resultado lo que nos da la funcion esPositivo y mandomos a esta funcion el numero, del cual desemaos saber si es positivo

    test(res); //Le mandamos el resultado a la funcion test.
}