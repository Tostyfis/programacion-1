//-------------------------------------------------------------------------------------------
//
//Programa; PorTres
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
// La función porTres() recibe un número real y devuelve el resultado de multiplicarlo por tres. Su diseño es el siguiente:
//
//Diseño;
//
// Real --> porTres() ---> Real
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion porTres()
//-------------------------------------------------------------------------------------------
int porTres(int numero){ //Creamos a funcion porTres

    int res = 0;

    res = numero*3; //Guardamos en lavarible res el resultado de multiplicar numero (El numero que le pasamos desde main) por tres.

    return res; //Con retur devolvemos el resultado obtenido

}
//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
void test(int nuemro){ //Definimos la funcion test para ver si todo esta correcto, en ella definimos la varible nuemro que es el valor que nos devuelve la funcion porTres

    if(nuemro != 3){ //Creamos un condicional (Si la varible numero es distinto de 3, imprimimos un ERROR)

        cout<<"ERROR"<< "\n";;

    }else{//Para todo lo demas imprimimos OK
        cout <<"OK" << "\n";
    }
    

}
//-------------------------------------------------------------------------------------------
//Funcion main()
//-------------------------------------------------------------------------------------------
int main(){

    int res = 0;

    int numeroQueIntroducimos = 1; 

    res = porTres(numeroQueIntroducimos); //Llamamos la funcion porTres pasandole un numero

    test(res); //Le pasamos a la funcion test el resultado obtendio en la anterior llamada 

}