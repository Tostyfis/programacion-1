//-------------------------------------------------------------------------------------------
//
//Programa; sumatorio pares
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
//Diseña una función sumatorioPares() que reciba un número natural n y devuelva la suma de los números naturales pares menores o iguales que n.
//
//Diseño;
//
// Natural --> suamtorioPares() ---> Natural
//
//-------------------------------------------------------------------------------------------
//
//Librerias;
//
#include "iostream"
//
using namespace std;
//
//-------------------------------------------------------------------------------------------
//Funcion sumatorio()
//-------------------------------------------------------------------------------------------
int sumatorioPares(int numero){ 

    int res = 0;

    for(int i = 0; i <= numero; i++){

        if(i%2 == 0){ //Creamos un condicional si el resto de i entre 2 es igual a 0 (quie decir si es par)

            res = i + res; //Realizamos la operacion

        }
 
    }

    return res;

}
//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
    void test(int resultado){

        if(resultado =! 3){

            cout << "Error";

        }else{

            cout << "OK";

        }
    
    }
//-------------------------------------------------------------------------------------------
int main(){

    int numero = 4; 

    int res = 0;

    res = sumatorioPares(numero);

    test(res); 

}

