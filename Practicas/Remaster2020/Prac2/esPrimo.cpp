//-------------------------------------------------------------------------------------------
//
//Programa; esPrimo
//
//Autor; Juan Ferrera Sala
//
//Uso; Libre
//
//Enunciado;
//
//Diseña una función esPrimo() que reciba un numero natural y devuelva verdadero si éste es un número primo.
//
//Diseño;
//
// Natural --> esPrimo() ---> V/F
//
//-------------------------------------------------------------------------------------------

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//-------------------------------------------------------------------------------------------
// Funcion esPrimo.
//-------------------------------------------------------------------------------------------

bool esPrimo(int a){

    if (a == 0 || a == 1 || a == 4){ // AND logico = ||      OR lofgico = &&

        return false; // en caso de que se culpla esta condicion

    }

    for (int x = 2; x < a / 2; x++) {

        if (a % x == 0) return false;

    } // Si no se pudo dividir por ninguno de los de arriba, el resultado es primo
  
    return true;

}

//-------------------------------------------------------------------------------------------
//Funcion test()
//-------------------------------------------------------------------------------------------
    void test(bool resultado){

        if(resultado =! true){

            cout << "El resultado es NO PRIMO " << "\n";  

        }else{

           cout << "El resultado es PRIMO " << "\n";  

        }
    
    }

//-------------------------------------------------------------------------------------------
// Funcion main.
//-------------------------------------------------------------------------------------------

int main(){

    bool res = 0; 

    int numero = 7;

    res = esPrimo(numero); 

    test(numero);

}