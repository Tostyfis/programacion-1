// ---------------------------------------------------

// Conjunto.h

// ---------------------------------------------------

#ifndef CONJUNTO_YA_INCLUIDO

#define CONJUNTO_YA_INCLUIDO

#include <iostream>

// ---------------------------------------------------
// ---------------------------------------------------

class Conjunto {

private:

int elementos[8]={0,0,0,0,0,0,0,0};

int elemento;

int donde(const int) const;

public:

Conjunto();

Conjunto(const int);

int vaciar();

unsigned int talla() const;

bool contiene(const int) const;

int anyadir(int);

int eliminar(int);

int unir(int,int);


}; // class

// ---------------------------------------------------

#endif
