// ---------------------------------------------------

// Conjunto.cpp

// g++ -c Conjunto.cpp

// ---------------------------------------------------

#include "Conjunto.h"

// ---------------------------------------------------
// ---------------------------------------------------

Conjunto::Conjunto()
    : elemento(0)
{}

// ---------------------------------------------------
// ---------------------------------------------------

Conjunto::Conjunto(const int numero)
    : elemento(numero)
{}

// ---------------------------------------------------
// ---------------------------------------------------

int Conjunto::donde(const int numero)const{

    int posicion = 0;

    for(int i = 0; i<= 8; i++){

        if(numero == elementos[i]){

            posicion = i;

        }
        
    }

    return posicion;

}
// ---------------------------------------------------
// ---------------------------------------------------

bool Conjunto::contiene(const int numero)const{

    int res = Conjunto::donde(numero);

    bool vOf;

    if(res <= 0 ){

        vOf = false;

    }else{

        vOf = true;

    }

    return vOf;
}

// ---------------------------------------------------
// ---------------------------------------------------
int Conjunto::vaciar(){

    for(int i =0; i <= 8; i++){

        elementos[i] = 0;

    }
    return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------

unsigned int Conjunto::talla() const {

    for(int i = 0; i<= 8; i++){

        std::cout<<elementos[i]<<"\n";

    }

int numero = 0;

int contador = 0;

for(int i =0; i <= 8; i++){

       numero  = elementos[i];

       if(numero != 0){

           contador++;

       }

    }

return numero;

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

int Conjunto::anyadir(int numero){


    bool res = Conjunto::contiene(numero);

    int guardado;

    if(res == 1){

        std::cout<<"El numero esta repetido"<<"\n";

    }if(res == 0){

        if(elementos[0]==0){

            elementos[0] = numero;

        }else{

                std::cout<<"OK anyadir"<<"\n";
        }
        
    }
    return 0;
}


// ---------------------------------------------------
// ---------------------------------------------------

int Conjunto::eliminar(int numero){


    bool res = Conjunto::donde(numero);

   
    if(res <= 0 ){

        std::cout<<"No se ha encontrado elemento" <<"\n";

    }else{

        std::cout<<"Eliminado" <<"\n";


        elementos[res] = 0;

    }
    
    return 0;
}

// ---------------------------------------------------
// ---------------------------------------------------

int Conjunto::unir(int numero, int numero2){

    int res = numero + numero2;
   
    Conjunto::anyadir(res);

    return 0;
}