// ---------------------------------------------------

// main.cpp

// g++ Conjunto.cpp main.cpp

// ---------------------------------------------------

#include <iostream>

#include "Conjunto.h"

// ---------------------------------------------------
// ---------------------------------------------------

void probarTalla() {

Conjunto c1;

unsigned int a = c1.talla();

std::cout << a << "\n";

if ( a != 0 ) {

std::cout << " mmm, parece que no va bien talla()\n";

return; // pues termino

}
// tal vez escribir más casos ...

std::cout << " parece que talla() va bien \n";

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

void probarContiene() {

Conjunto c1;

bool res = c1.contiene(4);

std::cout << res << "\n";



} // ()

// ---------------------------------------------------
// ---------------------------------------------------
void probarAnyadir(){

Conjunto c1;

c1.anyadir(4);

}

// ---------------------------------------------------
// ---------------------------------------------------
void probarEliminar() {

Conjunto c1;

c1.eliminar(4);

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

void probarUnir() {

Conjunto c1;

c1.unir(4,3);

} // ()

// ---------------------------------------------------
// ---------------------------------------------------

int main() {

probarTalla();

probarContiene();

probarAnyadir();

probarEliminar();

probarUnir();

} // ()