#include <iostream>

using namespace std;

//=====================================================================

// N ===> factorial() ===> N

//=====================================================================
int factorial(int numero){

    int resultado = 1;

    if(numero < 0){

        cout <<"No se puede calcular un factorial de un numero negativo " << "\n";

    }else{

       for(int i = numero; i > 0; i--){

        resultado = resultado * i;   

       }

    }

    return resultado;
}

int main(){

 int numero = 5;

 int resultado = factorial(numero);

 cout << "El resultado es " << resultado <<"\n";

}