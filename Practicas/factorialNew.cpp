#include <iostream>

using namespace std;


int multiplica(int numero1, int numero2){

    int res = 0;

    int p1,p2 = 0; //Se uqeda con la parte positiva

    if(numero1<0){

        p1 = numero1 * (-1);

    }else{
        p1 = numero1;
    }

    if(numero2<0){

        p2 = numero2 *(-1);

    }else{
        p2 = numero2;
    }
    
    for(int i = 0; i <= p1; i++){

        res = res + p2;

    }

    if((numero1 < 0 && numero2 < 0)  || (numero1 >= 0 && numero2>= 0)){

        return res;

    }

    if((numero1 < 0 && numero2 >= 0)  || (numero1 >= 0 && numero2 < 0)){

        return res*(-1);

    }

    
}

int dividir(int dividendo, int divisor){

    int contador = 0;

    while (dividendo >= divisor){

        dividendo = dividendo - divisor;

        contador++;

    }
    
    return contador;

}

int resto_dividir_entera(int dividendo, int divisor){

    while (dividendo >= divisor){

        dividendo = dividendo - divisor;

    }
    
    return dividendo;

}

int main(){

int n1,n2,res;

cout<<"Dame un numero entero: " << "\n";

cin >> n1;

cout<<"Dame otro numero entero: " << "\n";

cin >> n2;

//res = multiplica(n1,n1);

res = dividir(n1,n2);

//res = resto_dividir_entera(n1,n2);

cout << "El resultado es: "<< res <<"\n";

}



