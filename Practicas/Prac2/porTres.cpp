/*
 La función porTres() recibe un número real y devuelve el resultado de multiplicarlo por tres. Su diseño es el siguiente:

main()====>porTres====>R
                  R<====


Atención: “devolver” no es escribir en pantalla.
. Atención: Hay dos tipos mutuamente excluyentes de funciones:
– las de entrada/salida: se comunican con el usuario (escriben en pantalla y/o leen de teclado). No hacen cálculos. Ejemplo: main().
– las de cálculo: no se comunican con el usuario (ni leen ni escriben), sólo calculan. Ejemplo: porTres().
*/

//==================================================================================================================================

#include "iostream" // Llamamos a las librerias.

using namespace std;

//==================================================================================================================================
// Funcion porTes.
//==================================================================================================================================

int porTres(int a){ // Definimos la funcion por tres y el valor que se pasa desde main (int a).

    return a * 3; // Delvolvemos el valor a que viene desde main y lo multiplicamos por 3. 

}

//==================================================================================================================================
// Funcion main.
//==================================================================================================================================

int main(){

    int res = 0; // Definimos que el resultado es 0.

    res = porTres(2); // Asignamos a la varible resltado el valor que nos propoorcina porTres.

    cout << "El resultado es " << res << "\n";  // Imprimimos por pantalla el resultado con un pequenyo texto.

}