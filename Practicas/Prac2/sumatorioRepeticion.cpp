/*

La función sumatorio() recibe un número natural n y devuelve la suma 1 + 2 + 3 + · · · + n.
Su diseño es:

Natural ===> sumatorio() ===> Natural

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std;

//=====================================================================================================================================

int sumatorio(int n){

    int res = 0;

    for(int i = 0; i <= n; i++){ //Recorremos cada uno de los numeros que componen al numero natural y realizamos es sumatorio

        res = res + i; // Esta vez se puede realizar gracias a un bucle.

    }

    return res;

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================
int main(){

    int resSumatorio = sumatorio(4);

//==================================================================================================================================
//Test
//==================================================================================================================================

    cout << resSumatorio << "\n";

    if(resSumatorio != 10){

        cout << "ERROR" << "\n";

    }else{

        cout << "OK" << "\n";

    }

}