/*
La función esPositivo() recibe un número entero y devuelve verdadero si es mayor o igual que cero; o falso en caso contrario. 
Su diseño es el siguiente:

Entero ===> esPositivo() ===> V/F

Escribe y prueba la función esPosivo() en un programa llamado EsPositivo.cpp.

*/

//==================================================================================================================================

#include "iostream"

using namespace std;

//==================================================================================================================================
//Funcion esPositivo()
//==================================================================================================================================

int esPositivo(int n){

    bool resultado;

    if(n >= 0){

        resultado = true;

    }

    else{

        resultado = false;

    }


    return resultado;

}

//==================================================================================================================================
//Funcion main()
//==================================================================================================================================

int main(){

int res = 0;

res = esPositivo(-5);

cout << "El resultado es " << res << "\n"; 

}