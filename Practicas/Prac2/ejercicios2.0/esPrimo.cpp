/*
Diseña una función esPrimo() que reciba un numero natural y devuelva verdadero si éste es un número primo. Un número natural es primo si es divisible únicamente por uno y por sí mismo.
Escribe un algoritmo sencillo para determinar si un número n es o no primo.
Escribe un programa EsPrimo.cpp para comprobar tu implementación de la función esPrimo().

Natural ===> esPrimo() ===> V/F



*/

//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
// Funcion esPrimo.
//==================================================================================================================================

int esPrimo(int x){

if(x==0 || x==1 || x==4){

    return false;

}

for(int i; i < x / 2 ; i++){

    if(x % i == 0){

        return false;
    }

}

return true;


}


//==================================================================================================================================
// Funcion main.
//==================================================================================================================================

int main(){

int res;

res = esPrimo(7);


cout << "El resultado es " << res << "\n";  

    if (res == true){

        cout << "El resultado es PRIMO " << "\n";  

    }

    if (res == false){

        cout << "El resultado es NO PRIMO " << "\n";  

    } 

}