/*
Diseña una función esPrimo() que reciba un numero natural y devuelva verdadero si éste es un número primo. Un número natural es primo si es divisible únicamente por uno y por sí mismo.
Escribe un algoritmo sencillo para determinar si un número n es o no primo.
Escribe un programa EsPrimo.cpp para comprobar tu implementación de la función esPrimo().

Natural ===> esPrimo() ===> V/F



*/

//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
// Funcion esPrimo.
//==================================================================================================================================

bool esPrimo(int a){

    if (a == 0 || a == 1 || a == 4){ // AND logico = ||      OR lofgico = &&

        return false; // en caso de que se culpla esta condicion

    }

    for (int x = 2; x < a / 2; x++) {

        if (a % x == 0) return false;

    } // Si no se pudo dividir por ninguno de los de arriba, el resultado es primo
  
    return true;

}


//==================================================================================================================================
// Funcion main.
//==================================================================================================================================

int main(){

    int res = 0; 

    res = esPrimo(7); 

//==================================================================================================================================
//Test
//==================================================================================================================================

    cout << "El resultado es " << res << "\n";  

    if (res == true){

        cout << "El resultado es PRIMO " << "\n";  

    }

    if (res == false){

        cout << "El resultado es NO PRIMO " << "\n";  

    }
    
    
}