/*
La función esPositivo() recibe un número entero y devuelve verdadero si es mayor o igual que cero; o falso en caso contrario. 
Su diseño es el siguiente:

Entero ===> esPositivo() ===> V/F

Escribe y prueba la función esPosivo() en un programa llamado EsPositivo.cpp.

*/

//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std;

//==================================================================================================================================
// Funcion esPositivo.
//==================================================================================================================================

int esPositivo(int a){ 

    bool verdaderoFalso; //Declaramos la varible que dara el resultado

    if(a >= 0){ // si lo el numero entero proporcionado es mayor o igual a cero

    verdaderoFalso = true; // el resultado es true ya que al ser el numero natural mayor o igual a cero tenemos que devolver true.

    }

    else{ // en caso de que no se cumpla la primera condicion saltamos a este else que asigna false a la varible que proporciona el resultado
        
        verdaderoFalso = false;

    }
    
    return verdaderoFalso; //Con el return enviamos el resultado 
}


//==================================================================================================================================
// Funcion main.
//==================================================================================================================================

int main(){

    int res = 0; // Definimos que el resultado es 0.

    res = esPositivo(5); // Asignamos a la varible resultado el valor que nos propoorcina porTres.

//==================================================================================================================================
//Test
//==================================================================================================================================

    cout << "El resultado es " << res << "\n";  // Imprimimos por pantalla el resultado con un pequenyo texto.

}