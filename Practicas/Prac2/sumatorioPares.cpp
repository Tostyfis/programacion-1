/*

La función sumatorio() recibe un número natural n y devuelve la suma 1 + 2 + 3 + · · · + n.
Su diseño es:

Natural ===> sumatorio() ===> Natural

Un algoritmo para esta función sin bucles es el siguiente. Véase que una fórmula matemática para calcular algo 
es también un algoritmo:


*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================

int sumatorio(int n){

    int nEntreDos = n /2;

    int resSumatorioTotal = nEntreDos*(nEntreDos+1);

    cout << resSumatorioTotal << "\n";

    return resSumatorioTotal;

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================
int main(){

    int resSumatorio = sumatorio(4);

//==================================================================================================================================
//Test
//==================================================================================================================================

    //cout << resSumatorio << "\n";

    if(resSumatorio != 6){

        cout << "ERROR" << "\n";

    }else{

        cout << "OK" << "\n";

    }

}