/*

Diseña una función sumatorioPares() que reciba un número natural n y devuelva la suma de los números naturales pares menores o iguales que n.
Un algoritmo para esta función podría ser:

Natural ===> sumatorio() ===> Natural


*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std;

//=====================================================================================================================================

int sumatorio(int n){

    int res = 0;

    for(int i = 0; i <= n; i++){ //Creamos un buqle para recorrer uno a uno los numeros que compenen al numero natural que pasamos a sumatorio

        if( i % 2 == 0 ){ //Introducimos esta condicion para identificar los numeros pares

            res = res + i; //Despues de hacer el filtro realizamos la suma de los numeros pares

        }

    }

    return res;

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================
int main(){

    int resSumatorio = sumatorio(4);

//==================================================================================================================================
//Test
//==================================================================================================================================

    cout << resSumatorio << "\n";

    if(resSumatorio != 6){

        cout << "ERROR" << "\n";

    }else{

        cout << "OK" << "\n";

    }

}