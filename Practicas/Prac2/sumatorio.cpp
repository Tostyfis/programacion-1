/*

La función sumatorio() recibe un número natural n y devuelve la suma 1 + 2 + 3 + · · · + n.
Su diseño es:

Natural ===> sumatorio() ===> Natural

Un algoritmo para esta función sin bucles es el siguiente. Véase que una fórmula matemática para calcular algo 
es también un algoritmo:

Datos de entrada: n : N
Datos de salida: N
Devolver n·(n+1)/2

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std;

//=====================================================================================================================================

int sumatorio(int n){

    int res = 0;

    res = n*(n+1)/2; //Tomamos esta formula para poder realizar el sumatorio sin necesidad de crear un bucle, ya que este ejercio no se 
                     // pueden emplear

    return res; //Devolvemos el resultado 

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================
int main(){

    int resSumatorio = sumatorio(4);

//==================================================================================================================================
//Test
//==================================================================================================================================

    cout << resSumatorio << "\n";

    if(resSumatorio != 10){

        cout << "ERROR" << "\n";

    }else{

        cout << "OK" << "\n";

    }

}