/*

Diseña, escribe su algoritmo e implementa una función llamada filtrar() (nombre del programa: Filtrar.cpp)
que, dada una lista de números reales, devuelva otra lista sólo con los valores positivos de la primera lista.

listaDeNumerosReales<R> ===> filtrar() ===> listaDeValoresPositivos<N>

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================
// filtar()
//=====================================================================================================================================

int filtrar(int * p, const unsigned int cuantos){

    int listaFiltrada[4]={0,0,0,0}; //Definimos la nueva lista

    for(int i = 0; i <= cuantos-1; i++){

        if(p[i]>=0){ //Creamos un condicional  (si el valor de la lista con puntero es mayor o igual que 0)

            int b = 0; //Creamos una nueva varible para tener la cuanta las nuevas posiciones de la lista

            listaFiltrada[b] = p[i]; //Asignamos el valor mayor que 0 a una nueva posicion de la nueva lista

            b++; //Sumamos uno a la varible que cuenta las nuevas posiciones de la lista

        }
           
    }

    return 0;

}


//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

    int listaDeNumerosReales[] = {-2,-1,1,2};

    filtrar( & listaDeNumerosReales[0], 4);

}