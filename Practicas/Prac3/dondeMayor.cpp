/*

Diseña, escribe su algoritmo e implementa una función llamada dondeEstaElMayor() (nombre del programa:
DondeMayor.cpp) que, dada una lista de números reales, devuelva la posición (número de casilla) donde está guardado el mayor valor.

    listaDeNumerosreales<R> ===> dondeEstaElMayor() ===> R

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================


//=====================================================================================================================================
// dondeEstaElMayor()
//=====================================================================================================================================

int dondeEstaElMayor(int * p, const unsigned int cuantos){

    int res = 0;

    int mayorNumeroregistrado = 0;

    
    for(int i = 0; i <= cuantos-1; i++){

        if (p[i] > mayorNumeroregistrado){

            mayorNumeroregistrado=p[i];

        }
    }
    return mayorNumeroregistrado;
}


//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

    int listaDeNumerosreales[] = {1,3,2,6,5,4,7,19,8};

    int resFinal = dondeEstaElMayor( & listaDeNumerosreales[0], 9 );

    if(resFinal != 19){

        cout << "El resultado es ERRONEO < " << resFinal;

    }else{
        
        cout << "El resultado es CORRECTO < " << resFinal;

    }
    
}