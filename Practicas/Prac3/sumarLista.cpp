/*

1. Diseña una función sumarLista() que reciba una lista de números enteros y devuelva su suma.

    listaDeEnteros<Z> ===> sumaDeEnteros() ===> Z

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================
// sumarLista()
//=====================================================================================================================================

int sumarLista(int * p, const unsigned int cuantos){ //Declaramos las variables del puntero

    int res = 0; //Definimos esta varible para almacenar el resultado de las sumas

    for( int i=0; i <= cuantos-1; i++){ // creamos un bucle que recorrela lista

        res = p[i] + res; //Se guarda en la varible resultado 

    }

    return res; 

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

    int resSumaLista; //Definimos una varible resSumaLista para luego poder hacer un test y verificar que todo este correcto

    int lista[]={-3,-2,-1,0,1,2,3}; //Definimos y creamos la lista

    resSumaLista = sumarLista( & lista[0], 7); //Asignamos a la variable resultado el resultado de la funcion, y apuntamos con el 
                                               //puntero a esta ultima mencionada

//=====================================================================================================================================
// Test
//=====================================================================================================================================

    if(resSumaLista != 0){

        cout << "El resultado es ERRONEO < ";

    }else{

        cout << "El resultado es CORRECTO < ";

    }
}