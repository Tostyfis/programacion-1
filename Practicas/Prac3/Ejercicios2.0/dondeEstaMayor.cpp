/*
Diseña, escribe su algoritmo e implementa una función llamada dondeEstaElMayor() (nombre del programa:
DondeMayor.cpp) que, dada una lista de números reales, devuelva la posición (número de casilla) donde está guardado el mayor valor.
*/
//==================================================================================================================================
//Disenyo: 
//
//  listaDeNumerosreales<R> ===> dondeEstaElMayor() ===> R
//
//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
//Funcion dondeEstaMayor()
//==================================================================================================================================

int dondeEstaMayor(int * p, const unsigned int cuantos){

    int mayorResultado = 0;

    int posicion = 0;

    for(int i = 0; i <= cuantos-1; i++){

        if(p[i] > mayorResultado){

            mayorResultado = p[i];

            posicion = i;

        }

    }

    return posicion;

 }

//==================================================================================================================================
//Funcion main()
//==================================================================================================================================

int main(){

    int res = 0;

    int listaDeReales[5] = {1,4,2,5,3};

    res = dondeEstaMayor(& listaDeReales[0], 5);

//==================================================================================================================================
//Test()
//==================================================================================================================================

    cout << "La posicion de la casilla con el mayor numero es " << res << "\n";

    if(res =! 3){

        cout << "ERROR " << "\n";

    }else{

        cout << "OK " << "\n";

    }

}