//
// Diseña1 una función sumarLista() que reciba una lista de números enteros y devuelva su suma.
//
//==================================================================================================================================
//Disenyo 
//
// Lista <Z> ===> sumarLista ===> Z
//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
//Funcion sumarLista()
//==================================================================================================================================

int sumarLista(int * p, const unsigned int cuantos){

    int suma = 0;

    for(int i = 0; i <= cuantos-1; i++){

        suma = p[i] + suma;

    }

    return suma;

}
//==================================================================================================================================
//Funcion main()
//==================================================================================================================================

int main(){

    int listaDeEnteros[6] = {-2,-1,1,4,3,5};

    int res = 0;

    res = sumarLista(& listaDeEnteros[0], 6);

//==================================================================================================================================
//Test()
//=================================================================================================================================

    if(res =! 10){

        cout << "Error" << "\n";

    }else{

        cout << "OK" << "\n";

    }
}
