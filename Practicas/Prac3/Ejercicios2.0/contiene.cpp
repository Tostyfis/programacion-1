/*
 Diseña, escribe su algoritmo e implementa una función llamada contiene() (nombre del programa: Contiene.cpp)
que, dada una lista de números enteros y un entero n, devuelva verdadero si la lista contiene a n.
*/
//==================================================================================================================================
//Disenyo: 
//
//  listaDeNumerosreales<R> ===> dondeEstaElMayor() ===> R
//
//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
//Funcion contiene()
//==================================================================================================================================

int contiene(int * p, const unsigned int cuantos, int entero){

    bool hayNumero;

    for(int i = 0; i <= cuantos-1; i++){

        if(p[i] == entero){

            hayNumero = true;

        }else{

            hayNumero = false;

        }

    }

    return hayNumero;

 }

//==================================================================================================================================
//Funcion main()
//==================================================================================================================================

int main(){

    int res = 0;

    int numeroEntero = 5;

    int listaDeEnteros[5] = {-3,-4,1,5,2};

    res = contiene(& listaDeEnteros[0], 5, numeroEntero);

//==================================================================================================================================
//Test()
//==================================================================================================================================

    cout << "resultado " << res << "\n";

    if(res =! 1){

        cout << "ERROR " << "\n";

    }else{

        cout << "OK " << "\n";

    }

}