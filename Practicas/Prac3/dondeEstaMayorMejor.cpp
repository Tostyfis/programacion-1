/*
Diseña, escribe su algoritmo e implementa una función llamada dondeEstaElMayor() (nombre del programa:
DondeMayor.cpp) que, dada una lista de números reales, devuelva la posición (número de casilla) donde está guardado el mayor valor.
*/
//==================================================================================================================================
//Disenyo: 
//
//  listaDeNumerosreales<R> ===> dondeEstaElMayor() ===> R
//
//==================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//==================================================================================================================================
//Funcion dondeEstaMayor()
//==================================================================================================================================

int dondeEstaMayor(int * p, const unsigned int cuantos){

    int mayorResultado = 0; //Creamos un espacio para guardar el mayor numero registrado

    int posicion = 0; //Creaomos otro espacio para alamcenar la posicion del mayor numero

    for(int i = 0; i <= cuantos-1; i++){ //El bucle tiene la funcion de recorrer toda la lista

        if(p[i] > mayorResultado){ //Creamos un condicional (Si p es mayor que el resultado guardado)

            mayorResultado = p[i]; //Asignamos el valor del puntero a la varible para almacenar datos

            posicion = i; // Y por ultimo guardamos el dato de la posicion 

        }

    }

    return posicion;

 }

//==================================================================================================================================
//Funcion main()
//==================================================================================================================================

int main(){

    int res = 0;

    int listaDeReales[5] = {1,4,2,5,3}; //Creamos la lista de numeros reales

    res = dondeEstaMayor(& listaDeReales[0], 5);

//==================================================================================================================================
//Test()
//==================================================================================================================================

    cout << "La posicion de la casilla con el mayor numero es " << res << "\n";

    if(res =! 3){

        cout << "ERROR " << "\n";

    }else{

        cout << "OK " << "\n";

    }

}