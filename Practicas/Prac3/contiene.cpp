/*

Diseña, escribe su algoritmo e implementa una función llamada contiene() (nombre del programa: Contiene.cpp)
que, dada una lista de números enteros y un entero n, devuelva verdadero si la lista contiene a n.

ListaDeEnteros<N> ===> N ===> contienne() ===> v/f

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================


//=====================================================================================================================================
// contiene()
//=====================================================================================================================================

int contiene(int * p, const unsigned int cuantos, int numeroEntero){

    bool res; //definimos la varible que guardara el resultado

    for(int i =0 ; i <= cuantos-1; i++){
        
        if(p[i] == numeroEntero){ //Creamos un condicional (Si el numaro de la lista es igual al numero entero que me pasan)

            res = true; //Guardamos en la varible res el valor true
        
        }

    }

    return res;
    
}


//=====================================================================================================================================
// main()
//=====================================================================================================================================

int main(){

    int listaDeEnteros[] = {1,2,3,4,5,6,7,8,9};
    
    int entero = 5;

    bool resFinal = contiene(& listaDeEnteros[0], 9, entero);

//=====================================================================================================================================
// test
//=====================================================================================================================================

    if(resFinal == true){

        cout << "Todo va bien" << "\n";

    }else{

        cout << "Ha ocurrido un error" << "\n";

    }
    
}