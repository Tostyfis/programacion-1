// ---------------------------------------------------
// 
// main.cpp
// 
// g++  Punto.cpp mainDentroCirculo.cpp 
// 
// ---------------------------------------------------

#include <iostream>

#include <math.h>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto2( Punto * lista3, const int numeros ) {

  for(int i = 0; i <= numeros-1; i++){

    std::cout << "(" << lista3[i].getX() << ", " << lista3[i].getY() << ")\n";

  }

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {
  double nx;
  
  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;
} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
//  Lista<Punto>
//   ->
//  Radio
//  centro
//  ->
//   puntosInteriores()
//  ->
//  Lista<Punto>
// ---------------------------------------------------
void puntosInteriores(Punto * ListaP, const int cuatos, Punto centroC, double radioCirculo){

Punto listaDePuntosAptos[10];

double guardado = 0;

int b = 0;

for(int i = 0; i <= cuatos-1; i++){

  guardado = centroC.distancia(ListaP[i]);

  if(guardado <= radioCirculo){

    listaDePuntosAptos[b] = ListaP[i];

    b++;

  }

}

muestraPunto2(&listaDePuntosAptos[0],b);

}



// ---------------------------------------------------
// ---------------------------------------------------
int main() {

Punto centroCirculo(0,0);

double radio = 5.7;

Punto listaDePuntos[5];

leeListaPuntos(& listaDePuntos[0],5);

puntosInteriores(&listaDePuntos[0],5, centroCirculo,radio);



} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
