// ---------------------------------------------------
// 
// main.cpp
// 
// g++  Punto.cpp mainDentroCirculo.cpp 
// 
// ---------------------------------------------------

#include <iostream>

#include <math.h>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto2( Punto * lista3, const int numeros ) {

  for(int i = 0; i <= numeros-1; i++){

    std::cout << "(" << lista3[i].getX() << ", " << lista3[i].getY() << ")\n";

  }

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {
  double nx;
  
  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;
} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
//  Lista<Punto>
//   ->
//  Radio
//  centro
//  ->
//   puntosInteriores()
//  ->
//  Lista<Punto>
// ---------------------------------------------------

void puntosInteriores(Punto * lista,const int cuantos, const Punto & centro, int rad){

Punto listaDePuntosCirculo[5];

Punto guardado;

double dx;

double dy;

double distanciaGuardada;

int b = 0;

for(int i = 0; i <= cuantos-1; i++){

  guardado = lista[i];

  dx = centro.getX() - guardado.getX();

  dy = centro.getY() - guardado.getY();

  distanciaGuardada = sqrt( dx*dx + dy*dy );

    if(distanciaGuardada <= rad){

    listaDePuntosCirculo[b] = lista[i];

    b++;

    }

}

muestraPunto2(&listaDePuntosCirculo[0],4);

}
// ---------------------------------------------------
// ---------------------------------------------------
int main() {

  Punto listaDePuntos[5];

  Punto centroCirculo (0,0);

  int radioCirculo = 3;

  leeListaPuntos(&listaDePuntos[0],4);

  puntosInteriores(&listaDePuntos[0],4,centroCirculo,radioCirculo);


} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
