/*
Diseña una función que sirva para ordenar 3 números enteros.

Implementa esta función en C asumiendo que los 3 números no están guardados de forma consecutiva en
un array de C. Nombre del fichero: Ordena3v1.cpp

Diseño

lista<Z> ===> ordena ====> lista<Z>

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================
//=====================================================================================================================================
// pruebaAutomatica()
//=====================================================================================================================================
void pruebaAutomatica(int * lista, const int numeros){

for( int i = 0; i<= numeros-1; i++){

    cout << lista[i] << "\n";

}
}


//=====================================================================================================================================
// Ordena3v1()
//=====================================================================================================================================

void ordena3v1(int * p, const unsigned int cuantos){

    int x,y;

    for( int i=0; i <= cuantos-1; i++){

        for(int j = i+1; j < cuantos-1; j++){

            x = p[i] + p[j];

            if(x > 0){

                y = p[j];

                p[j] = p[i];

                p[i] = y;

            }

        }

    }

    pruebaAutomatica(&p[0],3);

}


//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

int listaDeNumeros[] = {2,1,4};

ordena3v1(& listaDeNumeros[0],3);


}