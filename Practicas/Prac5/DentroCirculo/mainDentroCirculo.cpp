// ---------------------------------------------------
// 
// main.cpp
// 
// g++  Punto.cpp mainDentroCirculo.cpp 
// 
// ---------------------------------------------------

#include <iostream>

#include <math.h>

#include "Punto.h"

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto( const Punto & p ) {

  std::cout << "(" << p.getX() << ", " << p.getY() << ")\n";

} // ()

// ---------------------------------------------------
// Punto
// ->
//   muestraPunto()
// ---------------------------------------------------

void muestraPunto2( Punto * lista3, const int numeros ) {

  for(int i = 0; i <= numeros-1; i++){

    std::cout << "(" << lista3[i].getX() << ", " << lista3[i].getY() << ")\n";

  }

} // ()

// ---------------------------------------------------
//   leePunto()
// ->
// Punto
// ---------------------------------------------------

Punto leePunto() {
  double nx;
  
  double ny;

  std::cout << "dime coordenada X ";

  std::cin >> nx;

  std::cout << "dime coordenada Y ";

  std::cin >> ny;

  Punto nuevo( nx, ny );

  return nuevo;
} // ()

// ---------------------------------------------------
//   leeListaPuntos()
// ->
// Lista<Punto>
// ---------------------------------------------------

void leeListaPuntos( Punto * pLista, const int cuantos ) {

  for( int i=0; i<=cuantos-1; i++) {

	pLista[ i ] = leePunto();

  } // for

} // ()

// ---------------------------------------------------
// ---------------------------------------------------
//  Lista<Punto>
//   ->
//  Radio
//  centro
//  ->
//   puntosInteriores()
//  ->
//  Lista<Punto>
// ---------------------------------------------------
void puntosInteriores(Punto * ListaP, const int cuatos, Punto centroC, double radioCirculo){

Punto listaDePuntosAptos[10]; //Definimos la nueva lista de puntos

double guardado = 0; //Creamos una variable para guardar datos temporales

int b = 0;

for(int i = 0; i <= cuatos-1; i++){ //Iniciamos un bucle para recorrer la lista de puntos

  guardado = centroC.distancia(ListaP[i]);//Calculamos la distancia de los distintos puntos de la lista con respecto al centro del circulo y la almacenamos temporalmete

  if(guardado <= radioCirculo){ //Si la distancia del punto calculado es menor al radio

    listaDePuntosAptos[b] = ListaP[i]; //Guardamos el punto en la lista nueva

    b++;

  }

}

muestraPunto2(&listaDePuntosAptos[0],b); //Enviamos la lista y la varible b para que se envien solo los puntos calculados

}



// ---------------------------------------------------
// ---------------------------------------------------
int main() {

Punto centroCirculo(0,0); //Definimos el punto central del circulo

double radio = 5.7; //Definimos su radio

Punto listaDePuntos[5];

leeListaPuntos(& listaDePuntos[0],5);

puntosInteriores(&listaDePuntos[0],5, centroCirculo,radio); //mandamos a puntosInteriores la lista de puntos, el centro del circulo y el radio



} // ()

// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
// ---------------------------------------------------
