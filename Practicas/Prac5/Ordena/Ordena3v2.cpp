/*

Diseña una función que sirva para ordenar 3 números enteros.

Implementa otra versión de esta función en C asumiendo que los 3 números sí están guardados de forma consecutiva en un array.

Nombre del fichero: Ordena3v2.cpp

*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================
//=====================================================================================================================================
// pruebaAutomatica()
//=====================================================================================================================================

void pruebaAutomatica(int * lista, const int numeros){

int guardado1,guardado2,guardado3 = 0;

guardado1 = lista[0] ;

guardado2 = lista[1] ;

guardado3 = lista[2] ;

if(guardado1 = 1){

        if(guardado2 =2){

            if(guardado3 =3){

                cout << "OK" <<"\n";

            }

        }

    }else{

        cout << "ERROR" <<"\n";

            for(int b=0; b<= numeros-1 ;b++){

                printf("%d  ", lista[b]);
        

            }//for()
    
    }
}

//=====================================================================================================================================
// Ordena3v2()
//=====================================================================================================================================

int ordena3v1(int * p, const unsigned int cuantos){

    int x,y,z;

    x = p[0];

    y = p[1];

    z = p[2];

    int numeroGuardado = 0;

    int mayorNumero = 0;

    int menorNumero = 0;

    if (x > y){

        numeroGuardado = x;

        if(x > z){

            mayorNumero = numeroGuardado;

            p[2] = mayorNumero;

            

            if(y > z){

                menorNumero = z;

                p[0] = menorNumero;

                p[1] = y;

            }
        }

    }

    pruebaAutomatica(&p[0],3);

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

    int lista[]={3,2,1};

    ordena3v1(&lista[0],3);


}