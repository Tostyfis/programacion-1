/*
Diseña una función que sirva para ordenar 3 números enteros.

Implementa esta función en C asumiendo que los 3 números no están guardados de forma consecutiva en
un array de C. Nombre del fichero: Ordena3v1.cpp


*/

//=====================================================================================================================================

#include "iostream" // Llamamos al las librerias.

using namespace std; // Llamamos al las librerias.

//=====================================================================================================================================
//=====================================================================================================================================
// pruebaAutomatica()
//=====================================================================================================================================

void pruebaAutomatica(int * lista, const int numeros){

int guardado1,guardado2,guardado3 = 0;

guardado1 = lista[0] ;

guardado2 = lista[1] ;

guardado3 = lista[2] ;

if(guardado1 = 3){

        if(guardado2 =2){

            if(guardado3 =1){

                cout << "OK" <<"\n";

            }

        }

    }else{

        cout << "ERROR" <<"\n";

            for(int b=0; b<= numeros-1 ;b++){

                printf("%d  ", lista[b]);
        

            }//for()
    
    }
}

//=====================================================================================================================================
// Ordena3v1()
//=====================================================================================================================================

int ordena3v1(int * p, const unsigned int cuantos){

    int x,y;

    for( int i=0; i <= cuantos-1; i++){

        for(int j = i+1; j < cuantos-1; j++){

            x = p[i] + p[j];

            if(x > 0){

                y = p[j];

                p[j] = p[i];

                p[i] = y;

            }

        }

    }

    pruebaAutomatica(&p[0],3);

}

//=====================================================================================================================================
// MAIN()
//=====================================================================================================================================

int main(){

    int lista[]={2,3,1};

    ordena3v1(&lista[0],3);


}